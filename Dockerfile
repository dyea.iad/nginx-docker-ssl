FROM nginx:latest



COPY ./default.conf /etc/nginx/conf.d/default.conf

COPY ./certs/ssl-bundle.crt /etc/ssl/ssl-bundle.crt
WORKDIR /root/.ssh/
COPY ./certs/AC-CGI.key /root/.ssh/.
COPY ./www/*.* /usr/share/nginx/html/.

#Debugging tools
#RUN apt-get update
#RUN apt-get install -y curl
#RUN apt-get install -y htop
#RUN apt-get install -y nano

EXPOSE 80
EXPOSE 443
CMD ["nginx","-g","daemon off;"]
